package com.company;

public class PointArrayList {
    private Point[] points = new Point[0];

    public void addPoint(Point point) {
        Point[] newPoints = new Point[points.length+1];
        for (int i = 0; i < points.length; i++) {
            newPoints[i] = points[i];
        }
        newPoints[newPoints.length - 1] = point;
        points = newPoints;
    }

    public Point[] getPoints(){
        return points;
    }


}
