package com.company;

public class CircleContainer {
    private Circle circle = null;

    public Circle getCircle() {
        return circle;
    }

    public void setCircle(Circle circle) {
        this.circle = circle;
    }
}
