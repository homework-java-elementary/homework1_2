package com.company.menu;

import com.company.Point;
import com.company.PointArrayList;

import java.util.Scanner;

public class AddPointMenuItem implements MenuItem {
    private PointArrayList pointArrayList;
    private Scanner scanner;

    public AddPointMenuItem(PointArrayList pointArrayList, Scanner scanner) {
        this.pointArrayList = pointArrayList;
        this.scanner = scanner;
    }

    @Override
    public String getName() {
        return "Добавить точку";
    }

    @Override
    public void execute() {
        System.out.println("Введите координаты точки:");

        double x = getDoubleFromConsole("Введите x:");

        double y = getDoubleFromConsole("Введите y:");

        pointArrayList.addPoint(new Point(x, y));
    }

    private double getDoubleFromConsole(String text) {
        System.out.print(text);
        while (!scanner.hasNextDouble()) {
            scanner.nextLine();
            System.out.print(text);
        }
        return scanner.nextDouble();
    }
}
