package com.company.menu;

public class ExitMenuItem implements MenuItem{
    @Override
    public String getName() {
        return "Выход";
    }

    @Override
    public void execute() {
        System.out.println("Завершение работы программы ...");
    }

    @Override
    public boolean isFinal() {
        return true;
    }
}
