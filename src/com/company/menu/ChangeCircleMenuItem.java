package com.company.menu;

import com.company.Circle;
import com.company.CircleContainer;
import com.company.Point;

import java.util.Scanner;

public class ChangeCircleMenuItem implements MenuItem {
    Scanner scanner;
    CircleContainer circleContainer;

    public ChangeCircleMenuItem(CircleContainer circleContainer, Scanner scanner) {
        this.circleContainer = circleContainer;
        this.scanner = scanner;
    }

    @Override
    public String getName() {
        return "Изменить параметры окружности";
    }

    @Override
    public void execute() {
        System.out.println("Введите координаты центра окружности:");

        double x = getDoubleFromConsole("Введите x:", false);

        double y = getDoubleFromConsole("Введите y:", false);

        double r = getDoubleFromConsole("Введите радиус окружности:", true);

        circleContainer.setCircle(new Circle(new Point(x, y), r));
    }

    private double getDoubleFromConsole(String text, boolean onlyPositive) {
        if (onlyPositive) {
            double number;
            do {
                System.out.print(text);
                while (!scanner.hasNextDouble()) {
                    scanner.nextLine();
                    System.out.print(text);
                }
                number = scanner.nextDouble();
                scanner.nextLine();
            } while (number <= 0);
            return number;
        } else {
            System.out.print(text);
            while (!scanner.hasNextDouble()) {
                scanner.nextLine();
                System.out.print(text);
            }
            return scanner.nextDouble();
        }
    }
}
