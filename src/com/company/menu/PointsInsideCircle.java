package com.company.menu;

import com.company.Circle;
import com.company.CircleContainer;
import com.company.Point;
import com.company.PointArrayList;

public class PointsInsideCircle implements MenuItem {
    private final PointArrayList pointArrayList;
    private CircleContainer circleContainer;

    public PointsInsideCircle(PointArrayList pointArrayList, CircleContainer circleContainer) {
        this.pointArrayList = pointArrayList;
        this.circleContainer = circleContainer;
    }

    @Override
    public String getName() {
        return "Точки лежащие в окружности";
    }

    @Override
    public void execute() {
        if (pointArrayList.getPoints().length == 0) {
            System.out.println("Вы не ввели ни одной точки");
            return;
        }

        if (circleContainer.getCircle() == null) {
            System.out.println("Вы не ввели окружность");
            return;
        }

        System.out.println("Точки лежащие в окружности:");
        Point[] points = pointArrayList.getPoints();
        Circle circle = circleContainer.getCircle();
        for (Point point : points) {
            if (circle.containsPoint(point)) {
                System.out.println(point);
            }
        }
        System.out.println();
    }
}
