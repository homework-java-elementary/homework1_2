package com.company;

import com.company.menu.*;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        PointArrayList pointArrayList = new PointArrayList();
        CircleContainer circleContainer = new CircleContainer();
        Menu menu = new Menu(new MenuItem[]{
                new AddPointMenuItem(pointArrayList, scanner),
                new ChangeCircleMenuItem(circleContainer, scanner),
                new PointsInsideCircle(pointArrayList, circleContainer),
                new PointsOutsideCircle(pointArrayList, circleContainer),
                new ExitMenuItem()
        }, scanner);
        menu.execute();
    }
}
